package ${pack};

import ${basepack}.support.BaseService;
import ${entity_pack}.${className};
import org.springframework.stereotype.Service;

/**
 * @author zengchao
 * @version 1.0
 * @since 1.0
 */
@Service
public class ${className}Service extends BaseService<${className}>{

}
